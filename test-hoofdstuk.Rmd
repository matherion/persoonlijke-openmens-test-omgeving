---
output: html_document
editor_options: 
  chunk_output_type: console
---

# Testhoofdstuk {#test-hoofdstuk}

```{r test-hoofdstuk-colofon, eval=FALSE, echo=FALSE, results="asis"}

md4e::knitWithClass(
  
  "Auteurs: Gjalt-Jorn Peters; laatste update: 2022-10-11",

  class="colofon"
)

```

```{r factor-analyse-summary, echo=FALSE, results="asis"}

md4e::knitListWithClass(
  
  prefix="In dit hoofdstuk wordt besproken:",
  
  "Onderwerp 1",
  "Onderwerp 2",
  "Onderwerp 3",

  class="overview"
)

```

```{r factor-analyse-courses, echo=FALSE, results="asis"}

md4e::knitListWithClass(
  
  prefix="Deze stof wordt behandeld in de volgende Open Universiteitscursus(sen):",
  
  "Cursus 1 (PB9999)",

  class="courses"
)

```

```{r factor-analyse-dependencies, echo=FALSE, results="asis"}

md4e::knitListWithClass(
  
  prefix="Dit hoofdstuk bouwt voort op deze andere hoofdstukken:",
  
  "Hoofdstuk 1",
  "Hoofdstuk 2",
  
  class="dependencies"
)

```

## Inleiding

Dit is de inleiding bij het hoofdstuk.

## Volgende paragraaf

Met wat inhoud.
