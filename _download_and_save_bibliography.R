### If this throws an error, run:
# install.packages('ufs');
ufs::checkPkgs(ufs = "0.4");
ufs::zotero_download_and_export_items(
  group = "2387508",
  file = here::here("openmens.bib")
);
