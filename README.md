# Open Methodologie en Statistiek

Open Methodologie en Statistiek, OpenMenS, is een Open Access boek van de Open Universiteit. Naast secties over methodologie en statistiek bevat het secties over de operationele kant van wetenschappelijk onderzoek binnen de psychologie, zoals werken binnen de Algemene Verordening Gegevensbescherming en volgens de principes van Open Science. Het boek is beschikbaar via https://openmens.nl.
